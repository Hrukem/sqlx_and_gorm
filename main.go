package main

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"log"
)

func main() {
	s := "host=" + "127.0.0.1" +
		" port=" + "5432" +
		" user=" + "postgres" +
		" password=" + "postgres" +
		" dbname=" + "sqlxtrain" +
		" sslmode=disable"

	db, err := sqlx.Open("postgres", s)
	if err != nil {
		log.Fatal("error create connect to database: \n", err)
	}

	if err = db.Ping(); err != nil {
		log.Fatal("error Ping to database: \n", err)
	}

	schema := `create table place (
		country text,
		city text NULL,
		telcode integer);`
	res, err := db.Exec(schema)
	fmt.Println("res: ", res)

	cityState := "INSERT INTO place " + "(country, telcode) VALUES ($1, $2)"
	countryCity := "INSERT INTO place " +
		"(country, city, telcode) VALUES ($1, $2, $3)"
	db.MustExec("INSERT INTO place (country, telcode) VALUES ($1, $2)",
		"Norway", 444)
	db.MustExec(cityState, "Brazil", 999)
	db.MustExec(countryCity, "Abhaziya", "Suhumi", 4321)
}
